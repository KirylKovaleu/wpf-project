﻿using Caliburn.Micro;
using System.Collections.ObjectModel;
using WpfTestApplication.Models;

namespace WpfTestApplication.Add
{
    public class AddViewModel : Screen
    {
        private readonly IWindowManager _windowManager;

        private ObservableCollection<TestViewModel> _testModel;
        public TestViewModel _model { get; set; }

        public AddViewModel(IWindowManager windowManager,
            TestViewModel model,
            ObservableCollection<TestViewModel> testModel)
        {
            _windowManager = windowManager;
            if (model != null)
            {
                _model = model;
            }
            else
            {
                _model = new TestViewModel();
            }
            _testModel = testModel;
            DisplayName = "Add or Edit";
        }
        
        public void Save()
        {
            _testModel.Add(_model);
            TryClose();
        }

        public void Edit()
        {
            TryClose();
        }
    }
}
