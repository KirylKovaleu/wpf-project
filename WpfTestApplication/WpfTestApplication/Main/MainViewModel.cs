﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using WpfTestApplication.Add;
using WpfTestApplication.Models;
using WpfTestApplication.Services;

namespace WpfTestApplication.Main
{
    public class MainViewModel : Screen
    {
        public delegate void AddResponseModel(ResponseViewModel model);
        private AddResponseModel _addResponceModel;
        private HttpClient _client;
        private Task[] _thread;
        private ResultViewModel _resultModel;
        private ResponseViewModel _responseModel;
        private ProgressBarViewModel _progressBar;
        private List<ResponseViewModel> _respModel;
        private readonly IWindowManager _windowManager;
        private CancellationTokenSource _cancelTokenSource;
        private ObservableCollection<TestViewModel> _testModels;
        private ObservableCollection<ResponseViewModel> _responseModels;
        private readonly Func<TestViewModel, ObservableCollection<TestViewModel>, AddViewModel> _getAddModel;

        public MainViewModel(IWindowManager windowManager,
            Func<TestViewModel, ObservableCollection<TestViewModel>, AddViewModel> getAddModel,
            HttpClient client)
        {
            _client = client;
            _windowManager = windowManager;
            _getAddModel = getAddModel;
            _resultModel = new ResultViewModel();
            _responseModel = new ResponseViewModel();
            _progressBar = new ProgressBarViewModel();
            _respModel = new List<ResponseViewModel>();
            ResultsTimeRsponse = new List<long>();
            _responseModels = new ObservableCollection<ResponseViewModel>();
            AddUrlTest();
        }

        public ProgressBarViewModel ProgressBar
        {
            get { return _progressBar; }
            set
            {
                _progressBar = value;
                NotifyOfPropertyChange(() => ProgressBar.PbStatus);
            }
        }


        public List<long> ResultsTimeRsponse { get; set; }

        public Task[] Threads
        {
            get { return _thread; }
            set
            {
                _thread = value;
                NotifyOfPropertyChange(nameof(Threads));
            }
        }

        public ObservableCollection<TestViewModel> TestModels
        {
            get { return _testModels; }
            set
            {
                _testModels = value;
                NotifyOfPropertyChange(nameof(TestModels));
            }
        }

        public List<ResponseViewModel> RespModel
        {
            get { return _respModel; }
            set
            {
                _respModel = value;
                NotifyOfPropertyChange(()=>RespModel);
            }
        }

        public ObservableCollection<ResponseViewModel> ResponseModels
        {
            get { return _responseModels; }
            set
            {
                _responseModels = value;
                NotifyOfPropertyChange(()=>ResponseModels);
            }
        }

        public ResponseViewModel ResponseModel
        {
            get { return _responseModel; }
            set { _responseModel = value;

                NotifyOfPropertyChange(() => ResponseModel);
            }
        }

        public ResultViewModel ResultModel
        {
            get { return _resultModel; }
            set
            {
                _resultModel = value;
                NotifyOfPropertyChange(() => ResultModel);
            }
        }

        public void AddOrEdit()
        {
            var item = TestModels.FirstOrDefault(x => x.IsChange == true);
            var z = _windowManager.ShowDialog(_getAddModel(item, _testModels));
        }
        

        public async void Start()
        {
            _cancelTokenSource = new CancellationTokenSource();
            CancellationToken token = _cancelTokenSource.Token;
            
            _progressBar.pbStatus(GetSecond());

            foreach (var r in TestModels.Where(x=>x.IsChange == true))
            {
                r.Status = Status.Play;
                _thread = new Task[r.Threads];
                _addResponceModel += AddRespModel;

                for (int i = 0; i < _thread.Count(); i++)
                {
                    if(token.IsCancellationRequested)
                    {
                        return;
                    }
                      await PeriodicTaskFactory.Start(() =>
                    {
                       Timer(r.Url);
                    }, intervalInMilliseconds: 1000 / r.Rps, duration: r.Duration*1000);
                    
                }

                _resultModel.Threads += _thread.Count();
                _resultModel.MaxResponse = ResultsTimeRsponse.Max();
                _resultModel.MinResponse = ResultsTimeRsponse.Min();
                _resultModel.AverageResponse = ResultsTimeRsponse.Average();
                _resultModel.TotalRequests = ResultsTimeRsponse.Count;
            }
            
        }

        public async void Timer(object url)
        {
                Stopwatch time = new Stopwatch();

                time.Start();
                var tg = await _client.HttpClient.GetAsync(url.ToString());
                time.Stop();

                var result = time.ElapsedMilliseconds;
                ResultsTimeRsponse.Add(result);

                _responseModel.Url = url.ToString();
                _responseModel.Duration = result;

                if (tg.IsSuccessStatusCode)
                {
                _responseModel.SuccessfulResp = "+";
                _responseModel.FiledResp = "-";
                }
                else
                {
                _responseModel.SuccessfulResp = "-";
                _responseModel.FiledResp = "+";
                }

                _responseModel.Time = DateTime.Now;
                Application.Current.Dispatcher.Invoke(_addResponceModel, _responseModel);
        }

        public void AddRespModel(ResponseViewModel model)
        {
                _responseModels.Add(model);

            double revmoveBefor = model.Time.AddSeconds(-5).ToOADate();

            while (_responseModels.Count > 0 && _responseModels[0].Time.ToOADate() < revmoveBefor)
            {
                _responseModels.RemoveAt(0);
            }
        }
        
        public void Delete()
        {
           var items = _testModels.ToList();
           _testModels.Clear();
           items.RemoveAll(x => x.IsChange == true);
           
            foreach(var n in items)
            {
                _testModels.Add(n);
            }
        }

        public void DeleteAll()
        {
            _testModels.Clear();
        }

        public void Stop()
        {
            _cancelTokenSource.Cancel();

            _progressBar.PbStatus = 0;
            _respModel.Clear();
            _testModels.ToList().ForEach(x => x.Status = Status.Stop);
            _responseModels.Clear();
            _resultModel.Threads=0;
            _resultModel.TotalRequests = 0;
            _resultModel.MaxResponse = 0;
            _resultModel.MinResponse = 0;
        }

        public void ClearHistory()
        {
            _progressBar.PbStatus = 0;
            _respModel.Clear();
            _responseModels.Clear();
            _resultModel.Threads = 0;
            _resultModel.TotalRequests = 0;
            _resultModel.MaxResponse = 0;
            _resultModel.MinResponse = 0;
        }

        public int GetSecond()
        {
            int second = 0;
            var sec = from c in _testModels
                      where c.IsChange == true
                      select c.Duration * c.Threads * 1000;

            foreach(var t in sec)
            {
                second += t;
            }

            return second;

        }

        private void AddUrlTest()
        {
            _testModels = new ObservableCollection<TestViewModel>
            {
                new TestViewModel {IsChange=false, Url="http://localhost:34744/apievent/GetCities", Rps=5, Duration = 5, Status=Status.Stop, Threads=2 },
                new TestViewModel {IsChange=false, Url="http://localhost:34744/apievent/GetEvent", Rps=5, Duration = 5, Status=Status.Stop, Threads=2},
                new TestViewModel {IsChange=false, Url="http://localhost:34744/apievent/GetVenues", Rps=5, Duration = 5, Status=Status.Stop, Threads=2 },
                new TestViewModel {IsChange=false, Url="http://localhost:34744/apievent/GetDate", Rps=5, Duration = 5, Status=Status.Stop, Threads=2 },
                new TestViewModel {IsChange=false, Url="http://localhost:34744/apievent/Search", Rps=5, Duration = 5, Status=Status.Stop, Threads=2 },
                new TestViewModel {IsChange=false, Url="https://careers.epam.by/", Rps=5, Duration = 5, Status=Status.Stop, Threads=2 },
                new TestViewModel {IsChange=false, Url="http://autoby.by/", Rps=5, Duration = 5, Status=Status.Stop, Threads=2 }
            };
        }
    }
}
