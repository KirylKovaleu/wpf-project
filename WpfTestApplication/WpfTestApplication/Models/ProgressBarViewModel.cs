﻿using Caliburn.Micro;
using System.ComponentModel;
using System.Threading;

namespace WpfTestApplication.Models
{
    public class ProgressBarViewModel: Screen
    {
        private int _pbStatus;

        private int _second;

        public int Second
        {
            get { return _second; }
            set
            {
                _second = value;
                NotifyOfPropertyChange(() => Second);
            }
        }

        public int PbStatus
        {
            get { return _pbStatus; }
            set
            {
                _pbStatus = value;
                NotifyOfPropertyChange(() => PbStatus);
            }
        }

        public void pbStatus(int second)
        {
            _second = second;
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += worker_DoWork;
            worker.ProgressChanged += worker_ProgressChanged;

            worker.RunWorkerAsync();


        }

        void worker_DoWork( object sender, DoWorkEventArgs e)
        {
            for (int i = 0; i < 100; i++)
            {
                PbStatus++;
                Thread.Sleep(_second/100);
            }
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            PbStatus = e.ProgressPercentage;
        }
    }
}
