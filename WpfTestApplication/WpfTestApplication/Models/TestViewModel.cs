﻿using Caliburn.Micro;

namespace WpfTestApplication.Models
{
    public class TestViewModel: Screen
    {
        public int _threads;

        private bool _isChange;

        public string _url;

        public int _rps;

        public int _duration;

        public Status _status;

        public bool IsChange
        {
            get
            {
                return _isChange;
            }
            set
            {
                _isChange = value;
                NotifyOfPropertyChange(nameof(IsChange));
            }
        }

        public int Threads
        {
            get
            {
                return _threads;
            }
            set
            {
                _threads = value;
                NotifyOfPropertyChange(nameof(Threads));
            }
        }

        public string Url
        {
            get { return _url; }
            set
            {
                _url = value;
                NotifyOfPropertyChange(nameof(Url));
            }
        }

        public int Rps
        {
            get { return _rps; }
            set
            {
                _rps = value;
                NotifyOfPropertyChange(nameof(Rps));
            }
        }

        public int Duration
        {
            get { return _duration; }
            set
            {
                _duration = value;
                NotifyOfPropertyChange(nameof(Duration));
            }
        }

        public Status Status
        {
            get { return _status; }
            set
            {
                _status = value;
                NotifyOfPropertyChange(nameof(Status));
            }
        }
    }
}