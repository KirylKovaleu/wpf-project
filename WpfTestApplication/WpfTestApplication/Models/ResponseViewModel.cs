﻿using Caliburn.Micro;
using System;

namespace WpfTestApplication.Models
{
    public class ResponseViewModel
    {
        private string _url;
        private long _duration;
        private string _successfulResp;
        private string _filedResp;

        public DateTime Time { get; set; }

        public string Url
        {
            get { return _url; }
            set
            {
                _url = value;
            }
        }

        public long Duration
        {
            get { return _duration; }
            set
            {
                _duration = value;
            }
        }

        public string SuccessfulResp
        {
            get { return _successfulResp; }
            set
            {
                _successfulResp = value;
            }
        }

        public string FiledResp
        {
            get { return _filedResp; }
            set
            {
                _filedResp = value;
            }
        }
    }
}
