﻿using Caliburn.Micro;

namespace WpfTestApplication.Models
{
    public class ResultViewModel : PropertyChangedBase
    {
        private int _threads;
        private int _totalRequests;
        private int _totalErrors;
        private double _bandwidth;
        private long _minResponse;
        private long _maxResponse;
        private double _averageResponse;
        
        public int Threads
        {
            get { return _threads; }
            set
            {
                _threads = value;
                NotifyOfPropertyChange(() => Threads);
            }
        }

        public int TotalRequests
        {
            get { return _totalRequests; }
            set
            {
                _totalRequests = value;
                NotifyOfPropertyChange(nameof(TotalRequests));
            }
        }

        public double Bandwidth
        {
            get { return _bandwidth; }
            set
            {
                _bandwidth = value;
                NotifyOfPropertyChange(nameof(Bandwidth));
            }
        }

        public int TotalErrors
        {
            get { return _totalErrors; }
            set
            {
                _totalErrors = value;
                NotifyOfPropertyChange(nameof(TotalErrors));
            }
        }

        public long MinResponse
        {
            get { return _minResponse; }
            set
            {
                _minResponse = value;
                NotifyOfPropertyChange(nameof(MinResponse));
            }
        }

        public long MaxResponse
        {
            get { return _maxResponse; }
            set
            {
                _maxResponse = value;
                NotifyOfPropertyChange(nameof(MaxResponse));
            }
        }

        public double AverageResponse
        {
            get { return _averageResponse; }
            set
            {
                _averageResponse = value;
                NotifyOfPropertyChange(nameof(AverageResponse));
            }
        }
    }
}
