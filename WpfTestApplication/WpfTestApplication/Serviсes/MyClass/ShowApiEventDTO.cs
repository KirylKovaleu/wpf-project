// Code generated by Microsoft (R) AutoRest Code Generator 0.17.0.0
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.

namespace WpfTestApplication.Services.MyClass
{
    using System.Linq;

    public partial class ShowApiEventDTO
    {
        /// <summary>
        /// Initializes a new instance of the ShowApiEventDTO class.
        /// </summary>
        public ShowApiEventDTO() { }

        /// <summary>
        /// Initializes a new instance of the ShowApiEventDTO class.
        /// </summary>
        public ShowApiEventDTO(int? count = default(int?), EventShowDTO eventModel = default(EventShowDTO))
        {
            Count = count;
            EventModel = eventModel;
        }

        /// <summary>
        /// </summary>
        [Newtonsoft.Json.JsonProperty(PropertyName = "count")]
        public int? Count { get; set; }

        /// <summary>
        /// </summary>
        [Newtonsoft.Json.JsonProperty(PropertyName = "eventModel")]
        public EventShowDTO EventModel { get; set; }

    }
}
